
# Ejercicio: Ploteado de Latencias

Este repositorio contiene los datos/ficheros necesarios para realizar el ejercicio descrito en el [wiki](https://gitlab.eif.urjc.es/roberto.calvo/setr/-/wikis/PlotLatencias)

Dentro del directorio data encontrarás:

- `ide-nonrt.csv`: CSV con los costes y latencias en una Raspberry Pi (no RT), con el sistema en estado idle.
- `idle-rt.csv`: CSV con los costes y latencias en una Raspberry Pi (RT), con el sistema en estado idle.
- `hb-nonrt.csv`: CSV con los costes y latencias en una Raspberry Pi (no RT), utilizando hackbench.
- `hb-rt.csv`: CSV con los costes y latencias en una Raspberry Pi (RT), utilizando hackbench.